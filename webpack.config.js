const path = require('path');
const webpack = require("webpack");

module.exports = [
    {
        entry: {
            login: "./typescript/login.ts",
	    index: "./typescript/index.ts",
	    complete_teacher: "./typescript/register_complete.ts"
        },
        output: {
            path: path.resolve(__dirname, "./main/js/"),
            filename: "[name].min.js",
            publicPath: "/js/"
        },
        devtool: "source-map",
        module: {
            loaders: [
                {
                    test: /\.tsx?$/,
                    exclude: /(node_modules)/,
                    loaders: ["babel-loader", "ts-loader"]
                }
            ]
        },
        resolve: {
            extensions: [".webpack.js", ".web.js", ".ts", ".js"]
        },
        plugins: [
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false
                }
            })
        ]
    }
];
