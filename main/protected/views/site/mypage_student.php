<?php
/**
 * @var AuthInfo $authInfo
 */
$user_id = $authInfo->user_id;
Yii::log(print_r($user_id, true), CLogger::LEVEL_ERROR);
$student = Student::model()->findByPk($user_id);
?>

<!-- $すでに何らかの情報がStudentテーブルに登録されている時 -->
<?php if (isset($student)): ?>
    <form class="info" action="/site/mypageStudentDB" method="post">
        <div>
            <div class="label"><label for="user_id">ユーザーid : <?php echo $student->user_id; ?></label></div>
            <!-- readonlyにより、編集が不可能になる -->
            <input readonly="readonly" id="user_id" name="user_id">
        </div>
        <div>
            <div class="label"><label for="mail">メールアドレス : <?php echo $student->mail; ?></label></div><input id="mail" name="mail">
        </div>
        <div>
            <div class="label"><label for="sex">性別 : <?php echo $student->sex; ?></label></div><input id="sex" name="time">
        </div>
        <div>
            <!-- スペルを間違って登録したので、そのまま使う(birthdayの「h」が抜けている) -->
            <div class="birthday"><label>誕生日 : <?php echo $student->birtday; ?></label></div>
            <input id="birthday" name="birthday">
        </div>
        <div>
            <div class="school"><label>学校 : <?php echo $student->school; ?></label></div>
            <input id="school" name="school">
        </div>
        <div>
            <div class="grade"><label>学年 : <?php echo $student->grade; ?></label></div>
            <input id="grade" name="grade">
        </div>
        <input type="submit" value="編集">
    </form>

<?php else: ?>
    <?php
    $student = new Student();
    $student->user_id = $authInfo->user_id;
    $student->mail = $authInfo->mail;
    $student->save();
    ?>
    <!-- TODO:site/mypageStudentDB -->
    <form class="info" action="/site/mypageStudentDB" method="post">
        <div>
            <div class="label"><label for="user_id">ユーザーid : <?php echo $student->user_id; ?></label></div>
            <!-- readonlyにより、編集が不可能になる -->
            <input readonly="readonly" id="user_id" name="user_id">
        </div>
        <div>
            <div class="label"><label for="mail">メールアドレス : </label></div><input id="mail" name="mail">
        </div>
        <div>
            <div class="label"><label for="sex">性別 : </label></div><input id="sex" name="time">
        </div>
        <div>
            <div class="birthday"><label>誕生日 : </label></div>
            <input id="birthday" name="birthday">
        </div>
        <div>
            <div class="school"><label>学校 : </label></div>
            <input id="school" name="school">
        </div>
        <div>
            <div class="grade"><label>学年 : </label></div>
            <input id="grade" name="grade">
        </div>
        <input type="submit" value="登録">
    </form>
<?php endif; ?>

<!-- user_id,アドレス,性別,誕生日,学校,学年 -->

<!--  -->

<?php
//$student = Student::model()->findByPk($user_id);
//Yii::log(print_r($student, true), CLogger::LEVEL_ERROR);
?>