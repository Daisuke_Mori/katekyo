<?php
/**
 * @var StudyForm $studyForm
 */
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/login.css?v=' . time());
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/login.min.js?v=' . time(), CClientScript::POS_END);
?>

    <body>
    <?php if (Yii::app()->user->getIsGuest()): ?>
        <div class="condition">未ログイン</div>
    <?php else: ?>
        <div class="condition">ログイン中</div>
    <?php endif; ?>

    <div class="login_form">
        <!-- KatekyoFormという配列でidとpwを6行目で指定したjsのファイルに渡す-->
        <div class="form_label"><label for="mail">メールアドレス : </label><input type="text" id="mail" name="PostedForm[mail]"/></div>
        <div class="form_label"><label for="password">パスワード : </label><input type="text" id="password" name="PostedForm[password]"/></div>
        <?php
        /*
         * エラーを表示する
        $msg=$studyForm->getError("username");
        if ($msg){
            echo "ユーザー名を入力してください。";
        }
        else{
            $msg=$studyForm->getError("password");
            echo "パスワードが間違っています。";
        }
        */
        ?>

        <div class="login_button">ログイン</div>
    </div>
    </body>

<?php