<?php
/**
 * $authInfoがAuthInfoクラスのインスタンスであることを明言する
 * これはIDEAに解釈させるため
 *
 * @var AuthInfo $authInfo
 */
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/index.min.js?v=' . time(), CClientScript::POS_END);
?>

<?php if(isset($authInfo)){
    $type = $authInfo->type;
}
?>

<header></header>
かてきょ
<ul>
    <li><a href="/site/index">トップページ</a></li>
    <li><a href="/site/about">かてきょについて</a></li>
    <li><a href="/site/teachers">講師を探す</a></li>
</ul>

<div>
    (サービスについての説明(簡単に))
</div>

<div>
    <ul>
        <?php if (!(Yii::app()->user->getIsGuest())): ?>
            <!-- クリックされたらtypescriptに制御が移り、ajaxを通してsiteControllerのactionLogoutが実行される、という流れ -->
            <li class="logout_button">ログアウト</li>
        <?php endif; ?>
        <?php if (Yii::app()->user->getIsGuest()): ?>
        <li><a href="/site/registerStudent">新規登録(生徒様、保護者の方)はこちら</a></li>
        <li><a href="/site/registerTeacher">新規登録(講師の方)はこちら</a></li>
        <?php endif; ?>
        <?php if (Yii::app()->user->getIsGuest()): ?>
            <li><a href="/site/login">マイページにログイン</a></li>
        <?php elseif ($type == "student"): ?>
            <li><a href="/site/mypageStudent">マイページ</a></li>
        <?php else : ?>
            <li><a href="/site/mypageTeacher">マイページ</a></li>
        <?php endif; ?>
    </ul>
</div>
