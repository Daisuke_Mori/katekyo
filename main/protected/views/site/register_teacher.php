<?php
/**
 * @var StudyForm $studyForm
 */
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/register.css?v=' . time());
?>

    <body>
    <div class="title">新規登録(講師の方)</div>
    <!-- action="/site/register"によってSiteControllerのactionRegister()が実行される(これはYiiの仕様) -->
    <form action="/site/registerTeacher" method="post">
        <!-- name="StudyForm[id]"により、postされた値はKatekyoFormという配列の[id]キーに対する値として登録される。2行目も同様 -->
        <div class="form_label"><label for="id">メールアドレス : </label><input type="text" id="mail" name="PostedForm[mail]"/></div>
        <div class="form_label"><label for="password">パスワード : </label><input type="text" name="PostedForm[password]"/></div>
        <div><input class="register" type="submit" value="登録"></div>
    </form>
    </body>
