<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/complete.css?v=' . time());
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/register_complete.min.js?v=' . time(), CClientScript::POS_END);
?>

<body>
<div class="complete">登録が完了しました。</div>
<div class="mypage_button">トップページへ</div>
</body>