<?php
/**
 * @var $content
 */
?>
<html>
<head>
    <link rel="stylesheet" href="/css/sanitize.css">
    <script type="text/javascript" src="/js/jquery-3.1.1.min.js"></script>
</head>
<body>
<?php echo $content; ?>
</body>
</html>