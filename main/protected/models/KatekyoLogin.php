<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class KatekyoLogin extends CFormModel
{
    public $mail;
    public $password;

    /**
     * Declares the validation rules.
     */
    //rules()内では仕様に乗っ取って定義すれば良い
    public function rules()
    {
        return array(
            // アドレスとpasswordが空欄でないことを調べる
            array('mail, password', 'required'),
            // 無効なアドレスでないか調べる
            array('mail', 'email'),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'verifyCode'=>'Verification Code',
        );
    }
}