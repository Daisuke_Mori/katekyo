<?php

class RegisterForm extends CFormModel{
    public $mail;
    public $password;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */

    /*
    public function rules()
    {
        return array(
            // username and password are required
            array('mail, password', 'required','message'=>'{attribute}を入力してください。'),
            // password needs to be authenticated
            array('password', 'sampleValidator','message'=>'{attribute}が間違っています。'),
        );
    }
    */

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'mail'=>'メールアドレス',
            'password'=>'パスワード'
        );
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */


    /*
    //rulesに関数を加える際には引数が2つ必要
    public function sampleValidator($attribute,$params)
    {
        //TODO このままだとNG
        //$this->addError('password','Incorrect username or password.');
    }
    */

    public function setParameters($form){
        if(isset($form["mail"])) $this->mail = $form["mail"];
        if(isset($form["password"])) $this->password = $form["password"];
    }
}