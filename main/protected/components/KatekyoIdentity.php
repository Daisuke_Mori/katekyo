<?php

//CUserIdentityはsrc/.yii-1.1.17.467ff50/framework/web/auth内
class KatekyoIdentity extends CUserIdentity
{
    //CUserIdentityは、$this>-usernameという引数と$this>-passwordという引数をpublicで定義している。
    //そのため、継承した時点で、$this>-usernameという引数と$this>-passwordという引数は定義されいている。

    //SiteControlletのactionIdPasswordから呼ばれた時点で、$this>-usernameという引数にidが、と$this>-passwordという引数にpasswordが渡される。
    public function authenticate()
    {
        //IdPasswordは外部で定義されているクラス。
        //参考:http://www.yiiframework.com/doc/guide/1.1/ja/database.ar
        //Yii::log(print_r($this->username, true), CLogger::LEVEL_ERROR);

        //データベースのクラスはhttp://192.168.33.12/gii/model/indexで登録する。
        /*このページに入るためのパスワードは、protected/config/main.phpの
        'modules'=>array(
            'gii'=>array(
         のところで設定する。
        なお、'ipFilters'=>array('*')のところでは、IPアドレスを指定。この指定だと、どのIPアドレスからでもアクセスを許可する。
        */

        //データベースにテーブルを作成するsqlファイルは、katekyo.sqlである。

        //AuthInfoはkatekyoデータベースのauth_infoテーブルのこと。これは上述のようにgiiで設定。
        //$this->usernameがuser_idである。
        /**
         * @var AuthInfo $authInfo
         */
        $authInfo = AuthInfo::model()->findByAttributes(array("mail" => $this->username));
        //$idPasswordは、primary keyの情報を基にしてデータベースから取り出された内容を属性として持つインスタンス
        Yii::log(print_r($authInfo, true), CLogger::LEVEL_ERROR);
        if (isset($authInfo)) {
            $password1 = $authInfo->pass_hash;
            //$this->passwordは、SiteControllerのactionIdPasswordから呼ばれた時に渡される、入力されたパスワード。
            $password2 = hash('sha256', $this->password . "salt");
            // $password1と$password2が等しければtrueを返す、という構文
            return $password1 === $password2;
        } else {
            return false;
        }
    }
}