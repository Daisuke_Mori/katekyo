<?php

class SiteController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        // ユーザーのタイプを渡す
        if (Yii::app()->user->getIsGuest()) {
            $this->layout = "//layouts/base";
            $this->render('index');
        } else {
            $id = Yii::app()->user->id;
            //Yii::log(print_r(Yii::app()->user->id, true), CLogger::LEVEL_ERROR);
            //mailが$idである
            $authInfo = AuthInfo::model()->findByAttributes(["mail" => $id]);
            //Yii::log(print_r($authInfo, true), CLogger::LEVEL_ERROR);
            //index.phpへ
            $this->layout = "//layouts/base";
            $this->render('index', ["authInfo" => $authInfo]);
        }
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $contactForm = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $contactForm->setAttributes($_POST['ContactForm']);
            if ($contactForm->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($contactForm->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($contactForm->subject) . '?=';
                $headers = "From: $name <{$contactForm->email}>\r\n" .
                    "Reply-To: {$contactForm->email}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $contactForm->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $contactForm));
    }

    public function actionKatekyoLogin()
    {
        //Yii::log(print_r("aaa", true), CLogger::LEVEL_ERROR);
        //models.KatekyoLogin.phpを参照
        $katekyoLogin = new KatekyoLogin;
        //ajaxで送られて来た値を取得
        $mail_password = $this->getJsonRequest();
        Yii::log(print_r($mail_password, true), CLogger::LEVEL_ERROR);
        if (isset($mail_password)) {
            $katekyoLogin->setAttributes($mail_password);
            //validate()はKatekyoLoginForm.phpのrules()に書かれているルールをチェックする。正しければtrueなら{}内が実行される。
            //詳しくは、飛んだ先の上(緑字)のメモを参照。
            if ($katekyoLogin->validate()) {
                $identity = new KatekyoIdentity($katekyoLogin->mail, $katekyoLogin->password);
                if ($identity->authenticate()) {
                    Yii::log(print_r("correct", true), CLogger::LEVEL_ERROR);
                    //30日間持続
                    $duration = 3600 * 24 * 30;
                    //ログインする
                    Yii::app()->user->login($identity, $duration);
                    //[result:true]という配列をJSON型の文字列にして返す
                    //下のreturn値はlogin.tsの$.ajaxのところへ
                    echo CJSON::encode(array("result" => true));
                } else {
                    Yii::log(print_r("incorrect", true), CLogger::LEVEL_ERROR);
                    echo CJSON::encode(array("result" => false));
                }
            } else {
                Yii::log(print_r("correct", true), CLogger::LEVEL_ERROR);
                echo CJSON::encode(array("result" => false));
            }
        } else {
            Yii::log(print_r("correct", true), CLogger::LEVEL_ERROR);
            echo CJSON::encode(array("result" => false));
        }
    }

    public function actionMessage()
    {
        $this->layout = "//layouts/base";
        $this->render('message');
    }

    public
    function actionMypageStudent()
    {
        # Yii::app()->user->idにより、ログインする時に打ち込んだメールアドレスが取得できる。これはYiiの仕様(?)
        $id = Yii::app()->user->id;
        //Yii::log(print_r(Yii::app()->user->id, true), CLogger::LEVEL_ERROR);
        //mailが$idである
        $authInfo = AuthInfo::model()->findByAttributes(["mail" => $id]);
        //Yii::log(print_r($authInfo, true), CLogger::LEVEL_ERROR);
        $this->layout = "//layouts/base";
        $this->render('mypage_student', ["authInfo" => $authInfo]);
    }

    public
    function actionMypageTeacher()
    {
        $id = Yii::app()->user->id;
        //Yii::log(print_r(Yii::app()->user->id, true), CLogger::LEVEL_ERROR);
        //mailが$idである
        $authInfo = AuthInfo::model()->findByAttributes(["mail" => $id]);
        //Yii::log(print_r($authInfo, true), CLogger::LEVEL_ERROR);
        $this->layout = "//layouts/base";
        $this->render('mypage_teacher', ["authInfo" => $authInfo]);
    }

    public
    function actionRegisterCompleteTeacher()
    {
        $this->layout = "//layouts/base";
        $this->render('register_complete_teacher');
    }

    public
    function actionRegisterCompleteStudent()
    {
        $this->layout = "//layouts/base";
        $this->render('register_complete_student');
    }

    public
    function actionTeachers()
    {
        $this->layout = "//layouts/base";
        $this->render('teachers');
    }

    public
    function actionAbout()
    {
        $this->layout = "//layouts/base";
        $this->render('about');
    }

    public
    function actionLogin()
    {
        $this->layout = "//layouts/base";
        $this->render('login');
    }


    /*
     *ログインするための関数、旧版
     *
    //この関数はlogin.tsのajaxのところで呼ばれる
    public function actionMailPassword()
    {
        //getJsonRequestは下で別に定義。
        //login.tsにおいて、ajaxを使ってpostされてきたデータを受け取り、getJsonRequestにより、JSONをPHPの配列に変換する。
        $mail_password = $this->getJsonRequest();
        //Yii::log(print_r($id_password, true), CLogger::LEVEL_ERROR);
        if (isset($mail_password["mail"])) {
            $mail = $mail_password["mail"];
            if (isset($mail_password["password"])) {
                $password = $mail_password["password"];
                //ここから先は、idもpasswordも空ではなかった時の処理。

                // config/main.phpで、protected/components内のファイルはすべて読み込まれる
                // KatekyoIdentityクラスは、component内のKatekyoIdentity.phpで定義されているので、読み込まれる
                $identity = new KatekyoIdentity($mail, $password);
                //id,passwordが合っている場合の処理
                //$identityはKatekyoIdentityクラスなので、KatekyoIdentity.phpで定義されている、authenticateという関数を持つ。

                //正しいパスワードが入力された場合
                if ($identity->authenticate()) {
                    Yii::log(print_r("correct", true), CLogger::LEVEL_ERROR);
                    //30日間持続
                    $duration = 3600 * 24 * 30;
                    //ログインする
                    Yii::app()->user->login($identity, $duration);
                    //[result:true]という配列をJSON型の文字列にして返す
                    //この戻り値はlogin.tsの$.ajaxのところへ
                    echo CJSON::encode(array("result" => true));
                    //間違っていた場合
                } else {
                    Yii::log(print_r("incorrect", true), CLogger::LEVEL_ERROR);
                    echo CJSON::encode(array("result" => false));
                }
                //パスワードが空だった場合
            } else {
                echo CJSON::encode(array("result" => false));
            }
            //アドレスが空だった場合
        } else {
            echo CJSON::encode(array("result" => false));
        }
    }
    */

    protected
    function getJsonRequest()
    {
        /*
         * getRequest()が見当たらない、と表示されることがある。これはIDEAの機能で、問題なく動く。
         * これを解決するには、アノテーションをつければ良い。
         * http://bashalog.c-brains.jp/15/12/07-171200.php
         * このサイトが参考になりそうだ。
        */
        //postされたデータを受けとる

        $rowData = Yii::app()->getRequest()->getRawBody();
        //Yii::log(print_r($rowData, true), CLogger::LEVEL_ERROR);
        //jsonで送られて来たデータをPHPの配列に変換
        return json_decode($rowData, true);
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
//ログアウト
    public
    function actionLogout()
    {
        Yii::app()->user->logout();
        //echoで返ってくる値は、呼び出し元のindex.tsへ
        echo CJSON::encode(array("result" => true));
    }

    /*
     * 新規登録(生徒)、旧版
     *
     public function actionRegisterStudent()
     {
         //Yii::log(print_r($_POST, true), CLogger::LEVEL_ERROR);
         //KatekyoFormはprotected/modelsで定義する。mailとpasswordの2つの属性を持つ。
         $registerForm = new RegisterForm();
         //$_POSTは配列として渡される。その中にさらにKatekyoForm配列がある。
         //Yii::log(print_r($_POST, true), CLogger::LEVEL_ERROR);
         //Yii::log(print_r($_POST['KatekyoForm'], true), CLogger::LEVEL_ERROR);
         if (isset($_POST['PostedForm'])) {
             //Yii::log(print_r("aaaaa", true), CLogger::LEVEL_ERROR);
             $postedForm = $_POST['PostedForm'];
             if (isset($postedForm['mail']) && $postedForm['password'] !== "") {
                 //Yii::log(print_r($_POST, true), CLogger::LEVEL_ERROR);
                 //setParametersはmodels/KatekyoForm.phpで設定している。
                 //配列の値を、registerFormクラスのインスタンスの属性として格納する
                 $registerForm->setParameters($_POST['PostedForm']);


                 // models/AuthInfo.phpにより、以下のコマンドで、auth_infoテーブルのインスタンスが生成される。
                 //上記のファイルは、giiで接続を設定した時に生成される。
                 $authInfo = new AuthInfo();
                 $authInfo->mail = $registerForm->mail;
                 $password_before_hash = $registerForm->password;
                 $authInfo->pass_hash = hash('sha256', $password_before_hash . "salt");
                 $authInfo->type = "student";
                 // このコマンドで保存できるのはYiiの機能
                 $authInfo->save();
                 //登録完了ページへ
                 $this->layout = "//layouts/base";
                 $this->render("register_complete_student");
             } else {
                 //register_student.phpを表示する関数。この関数の下で定義している。
                 $this->gotoRegisterStudent();
             }
         } else {
             $this->gotoRegisterStudent();
         }
     }
     */

    public
    function actionRegisterStudent()
    {
        //Yii::log(print_r($_POST, true), CLogger::LEVEL_ERROR);
        //KatekyoFormはprotected/modelsで定義する。mailとpasswordの2つの属性を持つ。
        $katekyoRegister = new KatekyoLogin();
        //$_POSTは配列として渡される。その中にさらにKatekyoForm配列がある。
        //Yii::log(print_r($_POST, true), CLogger::LEVEL_ERROR);
        //Yii::log(print_r($_POST['KatekyoForm'], true), CLogger::LEVEL_ERROR);
        if (isset($_POST['PostedForm'])) {
            //Yii::log(print_r("あああああ", true), CLogger::LEVEL_ERROR);
            $katekyoRegister->setAttributes($_POST['PostedForm']);
            if ($katekyoRegister->validate()) {
                //Yii::log(print_r($_POST, true), CLogger::LEVEL_ERROR);
                //setParametersはmodels/KatekyoForm.phpで設定している。
                //配列の値を、registerFormクラスのインスタンスの属性として格納する

                /* models/AuthInfo.phpにより、以下のコマンドで、auth_infoテーブルのインスタンスが生成される。
                上記のファイルは、giiで接続を設定した時に生成される。 */
                $authInfo = new AuthInfo();
                $authInfo->mail = $katekyoRegister->mail;
                $password_before_hash = $katekyoRegister->password;
                $authInfo->pass_hash = hash('sha256', $password_before_hash . "salt");
                $authInfo->type = "student";
                // このコマンドで保存できるのはYiiの機能
                $authInfo->save();

                //登録完了ページへ
                $this->layout = "//layouts/base";
                $this->render("register_complete");
            } else {
                //Yii::log(print_r("あああああ", true), CLogger::LEVEL_ERROR);
                //register_student.phpを表示する関数。この関数の下で定義している。
                $this->gotoRegisterStudent();
            }
        } else {
            $this->gotoRegisterStudent();
        }
    }

    public
    function gotoRegisterStudent()
    {
        $this->layout = "//layouts/base";
        $this->render("register_student");
    }

    public
    function actionRegisterTeacher()
    {
        //Yii::log(print_r($_POST, true), CLogger::LEVEL_ERROR);
        //KatekyoFormはprotected/modelsで定義する。mailとpasswordの2つの属性を持つ。
        $katekyoRegister = new KatekyoLogin();
        //$_POSTは配列として渡される。その中にさらにKatekyoForm配列がある。
        //Yii::log(print_r($_POST, true), CLogger::LEVEL_ERROR);
        //Yii::log(print_r($_POST['KatekyoForm'], true), CLogger::LEVEL_ERROR);
        if (isset($_POST['PostedForm'])) {
            //Yii::log(print_r("あああああ", true), CLogger::LEVEL_ERROR);
            $katekyoRegister->setAttributes($_POST['PostedForm']);
            if ($katekyoRegister->validate()) {
                //Yii::log(print_r($_POST, true), CLogger::LEVEL_ERROR);
                //setParametersはmodels/KatekyoForm.phpで設定している。
                //配列の値を、registerFormクラスのインスタンスの属性として格納する

                /* models/AuthInfo.phpにより、以下のコマンドで、auth_infoテーブルのインスタンスが生成される。
                上記のファイルは、giiで接続を設定した時に生成される。 */
                $authInfo = new AuthInfo();
                $authInfo->mail = $katekyoRegister->mail;
                $password_before_hash = $katekyoRegister->password;
                $authInfo->pass_hash = hash('sha256', $password_before_hash . "salt");
                $authInfo->type = "teacher";
                // このコマンドで保存できるのはYiiの機能
                $authInfo->save();
                //登録完了ページへ。この関数は下で定義。
                $this->gotoRegisterTeacher();
            } else {
                //Yii::log(print_r("あああああ", true), CLogger::LEVEL_ERROR);
                //register_student.phpを表示する関数。この関数の下で定義している。
                $this->gotoRegisterTeacher();
            }
        } else {
            $this->gotoRegisterTeacher();
        }
    }

    public
    function gotoRegisterTeacher()
    {
        $this->layout = "//layouts/base";
        $this->render("register_teacher");
    }

    //POSTされてきたデータをDBに登録する関数
    public function actionMypageStudentDB()
    {
        //StudentPostDataはこのスクリプトの下の部分で定義する。


        $student = new Student();
        $student->user_id = $_POST['user_id'];
        $student->mail = $_POST['mail'];
        $student->sex = $_POST['sex'];
        //スペルは$birtdayでOK(DB生成時のミスに起因する)
        $student->birtday = $_POST['birthday'];
        $student->school = $_POST['school'];
        $student->grade = $_POST['grade'];
        $student->save();
        $this->render("mypage_student");
    }


    public
    function actionInformationStudent()
    {
        $this->layout = "//layouts/base";
        $this->render('information_student');
    }

    public
    function actionInformationTeacher()
    {
        $this->layout = "//layouts/base";
        $this->render('information_teacher');
    }
}
