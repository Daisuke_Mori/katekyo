class CompletePageStudent {
    private $mypage_button: JQuery = $(".mypage_button");

    public init() {
        this.$mypage_button.click(() => {
            location.href = "/";
        })
    }
}

const completePageStudent = new CompletePageStudent();
completePageStudent.init();