//javascriptへのコンパイルは、同じtypescriptディレクトリのwebpack.config.jsonで設定する必要がある。
class LoginPage {
    private $login_button: JQuery = $(".login_button");
    public mail;
    public password;

    //初期化メソッド
    public init() {
        this.$login_button.click(() => {
            //console.log("clickされました。");
            /*参考:http://jszuki.doorblog.jp/archives/31702159.html

             (this.id = $(':text[name="KatekyoForm[id]"]').val();についての説明)

             たとえばHTMLが
             <input type="text" name="animalText">
             なら、値を取得するには
             var animal = $(':text[name="animalText"]').val();
             とすればよい。

             今回はtypeがtext,nameはKatekyoFormという配列の、キーがidのものの値を取得したいので、このように書く。
             */
            this.mail = $(':text[name="PostedForm[mail]"]').val();
            this.password = $(':text[name="PostedForm[password]"]').val();
            //console.log(this.mail);
            //console.log(this.password);

            //ajaxを使う
            $.ajax(<JQueryAjaxSettings>{
                //受取先に、ファイルの形式を伝えるヘッダ。
                contentType: "application/json",
                //送るデータ。
                //JSON.stringifyは、JavaScriptの値をJSON文字列に変換するメソッド。渡す前の値(カッコ内はjavascriptの配列)。
                data: JSON.stringify({mail: this.mail, password: this.password}),
                //受け取るデータの形式の指定。json形式で受け取る
                dataType: "json",
                //通信方法。今回はpostを指定しているので、urlで指定したところに、値を送る
                type: "post",
                //SiteControllerのKatekyoLoginを実行
                url: "/site/katekyoLogin",

                //SiteControllerのactionIdPasswordのreturn値がここに帰ってくる
            }).then((response) => {
                console.log(response);
                if (response.result) {
                    //トップページに飛ばす
                    //これはjavascriptの関数
                    location.href = "/";
                } else {
                    console.log("error");
                }
            }, (error) => {
                console.log(error);
            });
        });
    }
}

const loginPage = new LoginPage();
loginPage.init();