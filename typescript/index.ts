class Index_php {
    private $logout_button: JQuery = $(".logout_button");

    //初期化メソッド
    public init() {
        this.$logout_button.click(() => {
            //ログアウトの処理
            console.log("クリックされました。")
            $.ajax(<JQueryAjaxSettings>{
                dataType: "json",
                type: "post",
                //siteControllerの
                url: "/site/logout"
                //ajaxを使う。その反応が帰って来て以下が実行される
            }).then((response) => {
                console.log(response);
                //responseは配列。keyがresult,値はtrueであるはず。
                if (response.result) {
                    //トップページに飛ばす
                    location.href = "/";
                } else {
                    //console.log("エラーです。");
                }
            }, (error) => {
                console.log(error);
            });

            setTimeout(()=>{
                console.log("aaaa");
            },1000);


        })
    }
}

const index_php = new Index_php();
index_php.init();
